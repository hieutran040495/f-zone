//Function Ready
$(document).ready(function(){
  //Active menu
  $(".item_menu").click(function () {
    if(!$(this).hasClass('act_menu'))
    {
        $(".item_menu.act_menu").removeClass("act_menu");
        $(this).addClass("act_menu");        
    }
  });
  //Navbar header
  $(".hide_icon").hide();
  $(".shortcut").click(function(){
    if($(".list_menu").hasClass('fadeInLeft')) {
      $(".list_menu").addClass('animated fadeOutLeft');
      $(".list_menu").removeClass('fadeInLeft');
    }
    else if($(".list_menu").hasClass('fadeOutLeft')) {
      $(".list_menu").addClass(' animated fadeInLeft');
      $(".list_menu").removeClass(' fadeOutLeft');
    }
    else {
      $(".list_menu").addClass("animated fadeInLeft");
    }
    $(".hide_icon").toggle().addClass("animated rotateIn");
    $(".show_icon").toggle().addClass("animated rotateIn");
  });

  //Modal search
  $("#modal_search .modal-body a").click(function() {
      $(this).find("i").toggleClass('rotateUpModal');
  });
  //Modal login

  //Slide Home
   $(".regular").slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,

    autoplay: true,
    autoplaySpeed: 4000,
    speed: 1000,
    fade: true,
    cssEase: 'linear'
  });
 
  //Scroll Menu
  var position_menu = $(".menu").position();
  var y_menu = position_menu.top;

  var position_stt = $(".statistics").position();
  var y_stt = position_stt.top;

  var position_ab = $(".about").position();
  var y_ab = position_ab.top;

  var height_menu = parseInt($(".menu").height());
  var height_about = parseInt($(".about").height());
 
  var check = 0; 
 // var run_stt = y_stt + height_menu;
 var run_stt = y_stt - height_about;
  console.log('y_stt ' + y_stt);
  console.log('height_menu ' + height_menu);
  console.log('height_about ' + height_about);
  console.log('run_stt ' + run_stt);
  $(window).scroll(function() {
    var scrollTop = $(window).scrollTop();
     console.log(scrollTop);
      if (scrollTop > y_menu) {
       $('.menu').addClass('scroll_menu');
      }
      else {
       $('.menu').removeClass('scroll_menu');
      }
      if(check == 0) {
        if (scrollTop >= y_ab) {
         countUp();
         check++;
        }
      }
  });

   //Click href #
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': ($target.offset().top - height_menu )
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  //CountUp
  function countUp () {
    var options = {
      useEasing : true, 
      useGrouping : false, 
      separator : ',', 
      decimal : '.', 
      prefix : '', 
      suffix : '' 
    };
    var count1 = new CountUp("item_stt1", 0, 3200, 0, 2.5, options);
    var count2 = new CountUp("item_stt2", 0, 120, 0, 2.5, options);
    var count3 = new CountUp("item_stt3", 0, 360, 0, 2.5, options);
    var count4 = new CountUp("item_stt4", 0, 42, 0, 2.5, options);
    count1.start();
    count2.start();
    count3.start();
    count4.start();
  }

  //Edit menu - slide
  if($(window).innerWidth() <= 767) {
    $('.slide_home').css('margin-top', +height_menu +'px');
  }
  else {
    $('.slide_home').css('margin-top', '0px');
  }
  
})
//Function Resize
$(window).resize(function() {
  var height_menu = parseInt($(".menu").height());
  if($(window).innerWidth() <= 767) {
    $('.slide_home').css('margin-top', +height_menu +'px');
  }
  else {
    $('.slide_home').css('margin-top', '0px');
    if( $(".list_menu").hasClass('fadeOutLeft') )
     {
      $(".list_menu").removeClass('fadeOutLeft');
    }
  }
});
