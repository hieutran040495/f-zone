function config($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/home");
    $stateProvider
        //HOME
        .state('home', {
            url: '/home',
            templateUrl: 'app/components/home/home.html',
            controller: 'homeCtrl as home',
            data: {
                pageTitle: 'HOME'
            }
        })
        .state('detail_products', {
            url: '/detail_products',
            templateUrl: 'app/components/detail_products/detail_products.html',
            data: {
                pageTitle: 'DETAIL_NEWS'
            }
        })
        // .state('detail_restaurants', {
        //     url: '/detail_restaurants',
        //     templateUrl: 'app/components/detail_rest/detail_rest.html',
        //     data: {
        //         pageTitle: 'DETAIL_RESTAURANTS'
        //     }
        // })
        // .state('detail_restaurants', {
            
        //     url: '/detail_restaurants',
        //     templateUrl: 'app/components/detail_rest/detail_rest.html',
        // })
        .state('detail_restaurants', {
            url: '/detail_restaurants/:id',
            views: {
                '': {
                    templateUrl: 'app/components/detail_rest/detail_rest.html'
                },
                'main@detail_restaurants': {
                    templateUrl: 'app/components/detail_rest/main_detail_rest.html'
                }
            }

        })
        // .state('albums', {
        //     url: '/albums/:name',
        //     templateUrl: 'app/components/detail_rest/detail_albums/detail_albums.html',
        // })
        // .state('detail_restaurants.main', {
        //     url: '',
        //     templateUrl: 'app/components/detail_rest/main_detail_rest.html',
        // })
        // .state('detail_restaurants.albums', {
        //     url: '/albums/:name',
        //     templateUrl: 'app/components/detail_rest/detail_albums/detail_albums.html',
        // })
        
        $locationProvider.html5Mode(true);
}
app.config(config);