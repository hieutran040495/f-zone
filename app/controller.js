function mainCtrl() {

    //Function Resize
    $(window).resize(function() {
      var height_menu = parseInt($(".menu").height());
      if($(window).innerWidth() <= 767) {
        $('.content').css('margin-top', +height_menu +'px');
      }
      else {
        $('.content').css('margin-top', '0px');
        if( $(".list_menu").hasClass('fadeOutLeft') )
         {
          $(".list_menu").removeClass('fadeOutLeft');
        }
      }
    });
    //Scroll Menu
    // setTimeout(function(){ 
    //     var position_menu = $(".menu").position();
    //     var y_menu = position_menu.top;

    //     var position_stt = $(".statistics").position();
    //     var y_stt = position_stt.top;

    //     var position_ab = $(".about").position();
    //     var y_ab = position_ab.top;

    //     var height_menu = parseInt($(".menu").height());
    //     var height_about = parseInt($(".about").height());
     
    //     var check = 0; 
    //  // var run_stt = y_stt + height_menu;
    //     var run_stt = y_stt - height_about;
    //     $(window).scroll(function() {
    //         var scrollTop = $(window).scrollTop();
            
    //         if (scrollTop > y_menu) {
    //            $('.menu').addClass('scroll_menu');
    //           }
    //         else {
    //             $('.menu').removeClass('scroll_menu');
    //         }
    //         if(check == 0) {
    //             if (scrollTop >= y_ab) {
    //                 countUp();
    //                 check++;
    //             }
    //         }
    //   });

    //    //Click href #
    //     $('a[href^="#"]').on('click',function (e) {
    //         e.preventDefault();

    //         var target = this.hash;
    //         var $target = $(target);

    //         $('html, body').stop().animate({
    //             'scrollTop': ($target.offset().top - height_menu )
    //             }, 900, 'swing', function () {
    //             window.location.hash = target;
    //         });
    //     });
    //     //Edit menu - slide
    //     menu_top();
    //     function menu_top() {
    //         if($(window).innerWidth() <= 767) {
    //             $('.content').css('margin-top', +height_menu +'px');
    //         }
    //         else {
    //             $('.content').css('margin-top', '0px');
    //         }
    //     }
        
    // },1500);
}
function headerCtrl($scope) {

}
function menuCtrl() {
     //Active menu
    $(".item_menu").click(function () {
        if(!$(this).hasClass('act_menu'))
        {
            $(".item_menu.act_menu").removeClass("act_menu");
            $(this).addClass("act_menu");        
        }
    });
    //Navbar header
    
    $(".shortcut").click(function(){
        if($(".list_menu").hasClass('fadeInLeft')) {
            $(".list_menu").addClass('animated fadeOutLeft');
            $(".list_menu").removeClass('fadeInLeft');
        }
        else if($(".list_menu").hasClass('fadeOutLeft')) {
            $(".list_menu").addClass(' animated fadeInLeft');
            $(".list_menu").removeClass(' fadeOutLeft');
        }
        else {
            $(".list_menu").addClass("animated fadeInLeft");
        }
        $(".hide_icon").toggle().addClass("animated rotateIn");
        $(".show_icon").toggle().addClass("animated rotateIn");
    });
    $(".hide_icon").hide();
}
function slideHomeCtrl() {
    $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1000,
        fade: true,
        cssEase: 'linear'
    });
}
function homeCtrl($scope, $http) {
   
}
function newsCtrl() {

}
function restCtrl($scope, $http) {
     $http
        .get("http://localhost:3001/get_list_restaurant")
        .then(function (respone) {
            $scope.restaurants = respone.data.data.items;
            console.log($scope.restaurants);
        })
    $scope.local = 'http://localhost:8000/manage/upload/avata/';
}
function statisticsCtrl() {
    //CountUp
    countUp();
    function countUp () {
        var options = {
          useEasing : true, 
          useGrouping : false, 
          separator : ',', 
          decimal : '.', 
          prefix : '', 
          suffix : '' 
        };
        var count1 = new CountUp("item_stt1", 0, 3200, 0, 2.5, options);
        var count2 = new CountUp("item_stt2", 0, 120, 0, 2.5, options);
        var count3 = new CountUp("item_stt3", 0, 360, 0, 2.5, options);
        var count4 = new CountUp("item_stt4", 0, 42, 0, 2.5, options);
        count1.start();
        count2.start();
        count3.start();
        count4.start();
    }
}
function aboutCtrl() {

}
function contactCtrl() {

}
function footerCtrl() {

}
function modalLoginCtrl() {

}
function modalRegisterCtrl() {

}
function modalSearchCtrl() {

}
function detailProductsCtrl() {
    // $('.slider-for').slick();
    // mainCtrl();
    setTimeout(function(){ 
       //Slide ảnh khác của sp
        $('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true
        });
        $('.slider-nav').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          // dots: true,
          centerMode: true,
          focusOnSelect: true
        });
       //Slide ảnh liên quan
        $(".other_slide").slick({
            dots: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            fade: true,
            cssEase: 'linear'
        });
        // $(window).resize(function() { 
           
        // });
        change_slide();
        
    },1000);
    // $(window).resize(function() { 
    //     change_slide();   
    // });
    function change_slide (){
        if($(window).innerWidth() < 600) {
            $(".other_slide").slick({
                dots: false,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        } else {
             $(".other_slide").slick({
                dots: false,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4
            });
        }
    }
}
function modalRatingCtrl () {

    setTimeout(function(){ 
         // $(".rate1").rate();

            //or for example
            var options = {
                max_value: 5,
                step_size: 1,
                selected_symbol_type: 'hearts',
            }
            $(".rate1").rate(options);
        // var options = {
        //     max_value: 6,
        //     step_size: 1,
        //     selected_symbol_type: 'hearts',
        // }


        // $(".rate1").rate(options);

        // $(".rate1").on("change", function(ev, data){
        //     console.log(data.from, data.to);
        // });

        // $(".rate1").on("updateError", function(ev, jxhr, msg, err){
        //     console.log("This is a custom error event");
        // });

        // $(".rate1").rate("setAdditionalData", {id: 42});
        // $(".rate1").on("updateSuccess", function(ev, data){
        //     console.log(data);
        // });
    },1500);
       
}
function detailRestCtrl($scope, $stateParams, $http) {
    $scope.local = 'http://localhost:8000/manage/upload/avata/';
    var id = $stateParams.id;
    $http
        .get('http://localhost:3001/get_detail?member_id='+id)
        .then(function (respone) {
            $scope.dt_rest = respone.data.data.detail;
            console.log(respone.data.data.detail);
          // news.forEach(function (item, key) {
          //       var tintuc = item;
          //       var key = key;
          //       //Lấy 4 tin mới nhất
          //       if (key < 5 && key > 0) {
          //           topnews.push(tintuc);
          //       }
          //   });
                    
        });
    setTimeout(function(){ 
       //Slide ảnh khác của sp
      
        //Slide ảnh liên quan
        // $(".other_slide").slick({
        //     dots: false,
        //     infinite: true,
        //     slidesToShow: 4,
        //     slidesToScroll: 4,
        //     fade: true,
        //     cssEase: 'linear'
        // });
        change_slide();
        
    },1000);
    function change_slide (){
        if($(window).innerWidth() < 600) {
            $(".other_slide").slick({
                dots: false,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        } else {
             $(".other_slide").slick({
                dots: false,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4
            });
        }
    }
}
function albumsCtrl($scope, $stateParams, $http) {
    var id = $stateParams.id;
    $http
        .get('http://localhost:3001/get_list_menu?member_id='+id)
        .then(function (respone) {
            $scope.menu = respone.data.data.items;
            console.log(respone);
           console.log(respone.data.data.items);
        });
}
// function albumsCtrl($scope, $rootElement) {
//     $scope.detailAlbums = function() {
//         var main = $rootElement.find('.main');
//         main.css('display','none');
//     }
// }
function detailAlbumsCtrl($scope, $rootElement) {
    // setTimeout(function(){ 
    //     $(".tab_albums").slick({
    //         dots: false,
    //         infinite: true,
    //         slidesToShow: 6,
    //         slidesToScroll: 6
    //     });
    // },1500);

    // $scope.actTab = function() {
    //     // var text = $rootElement.find(this).text();
    //     // console.log(text);
       

    // }
    // $scope.test = {};
    // $scope.test.actTab = function() {
    //     var text = $rootElement.text();
    //     console.log(text);
       

    // }
    // $rootElement.find('item_tab').click(funtion() {

    // })
    $(".item_tab").click(function () {
        if(!$(this).hasClass('act_tab'))
        {
            $(".item_tab.act_tab").removeClass("act_tab");
            $(this).addClass("act_tab");        
        }
        var text = $(this).find("a").text();
        $('.title h2'). html(text);
        console.log(text);
    });
}
app.controller('mainCtrl', mainCtrl);
//Header
app.controller('headerCtrl', headerCtrl);
app.controller('menuCtrl', menuCtrl);
app.controller('slideHomeCtrl', slideHomeCtrl);
//Footer
app.controller('footerCtrl', footerCtrl);
//Home
app.controller('homeCtrl', homeCtrl);
app.controller('newsCtrl', newsCtrl);
app.controller('restCtrl', restCtrl);
app.controller('aboutCtrl', aboutCtrl);
app.controller('statisticsCtrl', statisticsCtrl);
app.controller('contactCtrl', contactCtrl);
//Modal
app.controller('modalLoginCtrl', modalLoginCtrl);
app.controller('modalRegisterCtrl', modalRegisterCtrl);
app.controller('modalSearchCtrl', modalSearchCtrl);
app.controller('modalRatingCtrl', modalRatingCtrl);
//Detail Products
app.controller('detailProductsCtrl', detailProductsCtrl);
//Detail Restaurants
app.controller('detailRestCtrl', detailRestCtrl);
app.controller('albumsCtrl', albumsCtrl);
app.controller('detailAlbumsCtrl', detailAlbumsCtrl);

