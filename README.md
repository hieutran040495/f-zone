﻿# HTML Structure

## Requirements installed

These are the minimum requirements for the project setup:

- [Node.js](http://nodejs.org) - `v4.4+`
- [Git](https://git-scm.com/)

## Getting started

Open your preferred command line tool and run follow some steps below:

1. __`git clone https://hieutran040495@bitbucket.org/hieutran040495/f-zone.git`__.
2. `cd f-zone`.
3. `npm install` automatically to install plugins required for the build script based in `package.json` file.
4. `gulp run` to preview and development. This command auto run url `http://localhost:3000` in your browser.



## Project structure

f-zone/  
>   assets/  
>  >  fonts
>
>  >  img
>
>  >  js
>
>  >  css
>
>  >  scss
>
>  >  less
>
package.json
>
index.html
>
gulpfile.js


## Requirements development
