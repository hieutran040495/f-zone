-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: restaurants
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `star` int(11) NOT NULL,
  `is_view` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_orders`
--

DROP TABLE IF EXISTS `customer_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `table_id` bigint(20) NOT NULL,
  `move_table_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_orders`
--

LOCK TABLES `customer_orders` WRITE;
/*!40000 ALTER TABLE `customer_orders` DISABLE KEYS */;
INSERT INTO `customer_orders` VALUES (155,6,1,30,NULL,1,0,1,'2016-11-17 13:30:23','2016-11-17 13:38:39'),(156,6,1,23,157,1,0,1,'2016-11-17 13:30:28','2016-11-17 14:43:33'),(157,6,1,27,NULL,1,0,0,'2016-11-17 13:30:35','2016-11-17 13:30:35'),(158,6,1,25,NULL,1,1,1,'2016-11-18 04:58:19','2016-11-18 06:56:10'),(159,6,1,29,NULL,1,1,1,'2016-11-18 04:58:49','2016-11-18 06:56:28'),(160,6,1,37,NULL,1,1,1,'2016-11-18 06:56:54','2016-11-18 12:27:59'),(161,6,1,31,NULL,1,1,1,'2016-11-18 12:26:59','2016-11-18 12:28:05'),(162,6,1,31,NULL,1,0,0,'2016-11-18 13:59:05','2016-11-18 13:59:05'),(163,6,1,25,NULL,1,1,1,'2016-11-18 18:00:01','2016-11-19 01:37:46'),(164,6,1,25,NULL,1,0,0,'2016-11-21 08:01:42','2016-11-21 08:01:42'),(165,6,1,26,NULL,1,1,1,'2016-11-24 07:30:24','2016-11-24 07:30:37');
/*!40000 ALTER TABLE `customer_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_orders_detail`
--

DROP TABLE IF EXISTS `customer_orders_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_orders_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `quality` int(11) NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_move` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_orders_detail`
--

LOCK TABLES `customer_orders_detail` WRITE;
/*!40000 ALTER TABLE `customer_orders_detail` DISABLE KEYS */;
INSERT INTO `customer_orders_detail` VALUES (1,1,4,1,0,NULL,'2016-11-12 16:20:15','2016-11-12 16:20:15'),(2,1,8,1,0,NULL,'2016-11-12 16:20:15','2016-11-12 16:20:15'),(3,2,4,1,0,NULL,'2016-11-12 16:34:25','2016-11-12 16:34:25'),(4,2,8,1,0,NULL,'2016-11-12 16:34:25','2016-11-12 16:34:25'),(5,3,4,1,0,NULL,'2016-11-13 06:42:52','2016-11-13 06:42:52'),(6,4,4,1,0,NULL,'2016-11-13 06:43:43','2016-11-13 06:43:43'),(7,5,4,1,0,NULL,'2016-11-13 06:44:25','2016-11-13 06:44:25'),(8,6,4,1,0,NULL,'2016-11-13 06:44:29','2016-11-13 06:44:29'),(9,7,4,1,0,NULL,'2016-11-13 06:44:35','2016-11-13 06:44:35'),(10,7,8,1,0,NULL,'2016-11-13 06:44:35','2016-11-13 06:44:35'),(11,8,4,1,0,NULL,'2016-11-13 06:44:45','2016-11-13 06:44:45'),(12,8,8,1,0,NULL,'2016-11-13 06:44:45','2016-11-13 06:44:45'),(13,8,9,2,0,NULL,'2016-11-13 06:44:45','2016-11-13 06:44:45'),(14,9,8,2,0,NULL,'2016-11-13 07:49:43','2016-11-13 07:49:43'),(15,10,8,2,0,NULL,'2016-11-13 09:01:07','2016-11-13 09:01:07'),(16,10,9,1,0,NULL,'2016-11-13 09:01:07','2016-11-13 09:01:07'),(17,11,8,2,0,NULL,'2016-11-13 09:02:50','2016-11-13 09:02:50'),(18,11,9,3,0,NULL,'2016-11-13 09:02:50','2016-11-13 09:02:50'),(19,12,4,2,0,NULL,'2016-11-13 09:03:24','2016-11-13 09:03:24'),(20,12,9,1,0,NULL,'2016-11-13 09:03:24','2016-11-13 09:03:24'),(21,13,4,1,0,NULL,'2016-11-13 09:05:04','2016-11-13 09:05:04'),(22,13,8,1,0,NULL,'2016-11-13 09:05:04','2016-11-13 09:05:04'),(23,13,9,1,0,NULL,'2016-11-13 09:05:04','2016-11-13 09:05:04'),(24,14,4,1,0,NULL,'2016-11-13 09:05:39','2016-11-13 09:05:39'),(25,15,8,1,0,NULL,'2016-11-13 09:07:42','2016-11-13 09:07:42'),(26,16,4,1,0,NULL,'2016-11-13 09:08:18','2016-11-13 09:08:18'),(27,17,4,2,0,NULL,'2016-11-13 09:10:13','2016-11-13 09:10:13'),(28,18,4,2,0,NULL,'2016-11-13 09:10:53','2016-11-13 09:10:53'),(29,18,9,2,0,NULL,'2016-11-13 09:10:53','2016-11-13 09:10:53'),(30,19,4,2,0,NULL,'2016-11-13 09:12:34','2016-11-13 09:12:34'),(31,19,8,2,0,NULL,'2016-11-13 09:12:34','2016-11-13 09:12:34'),(32,20,4,2,0,NULL,'2016-11-13 09:12:59','2016-11-13 09:12:59'),(33,20,8,2,0,NULL,'2016-11-13 09:12:59','2016-11-13 09:12:59'),(34,21,4,2,0,NULL,'2016-11-13 09:13:17','2016-11-13 09:13:17'),(35,21,8,2,0,NULL,'2016-11-13 09:13:17','2016-11-13 09:13:17'),(36,22,4,2,0,NULL,'2016-11-13 09:13:55','2016-11-13 09:13:55'),(37,22,8,2,0,NULL,'2016-11-13 09:13:55','2016-11-13 09:13:55'),(38,23,4,2,0,NULL,'2016-11-13 09:14:27','2016-11-13 09:14:27'),(39,23,8,2,0,NULL,'2016-11-13 09:14:27','2016-11-13 09:14:27'),(40,24,4,2,0,NULL,'2016-11-13 09:14:56','2016-11-13 09:14:56'),(41,24,9,4,0,NULL,'2016-11-13 09:14:56','2016-11-13 09:14:56'),(42,25,8,1,0,NULL,'2016-11-13 09:22:52','2016-11-13 09:22:52'),(43,26,8,1,0,NULL,'2016-11-13 09:23:11','2016-11-13 09:23:11'),(44,27,8,1,0,NULL,'2016-11-13 09:23:32','2016-11-13 09:23:32'),(45,28,4,1,0,NULL,'2016-11-13 09:46:27','2016-11-13 09:46:27'),(46,29,4,1,0,NULL,'2016-11-13 09:48:05','2016-11-13 09:48:05'),(47,30,4,1,0,NULL,'2016-11-13 09:50:09','2016-11-13 09:50:09'),(48,31,4,3,0,NULL,'2016-11-13 09:50:35','2016-11-13 09:50:35'),(49,32,4,7,0,NULL,'2016-11-13 09:51:08','2016-11-13 09:51:08'),(50,33,4,3,0,NULL,'2016-11-13 09:51:18','2016-11-13 09:51:18'),(51,34,4,1,0,NULL,'2016-11-13 10:04:58','2016-11-13 10:04:58'),(52,35,4,1,0,NULL,'2016-11-13 10:05:02','2016-11-13 10:05:02'),(53,36,4,1,0,NULL,'2016-11-13 10:05:04','2016-11-13 10:05:04'),(54,37,4,1,0,NULL,'2016-11-13 10:05:08','2016-11-13 10:05:08'),(55,38,4,1,0,NULL,'2016-11-13 10:05:11','2016-11-13 10:05:11'),(56,39,4,2,0,NULL,'2016-11-13 10:10:55','2016-11-13 10:10:55'),(57,39,8,2,0,NULL,'2016-11-13 10:10:55','2016-11-13 10:10:55'),(58,40,4,2,0,NULL,'2016-11-13 10:10:56','2016-11-13 10:10:56'),(59,40,8,2,0,NULL,'2016-11-13 10:10:56','2016-11-13 10:10:56'),(60,41,4,2,0,NULL,'2016-11-13 10:10:57','2016-11-13 10:10:57'),(61,41,8,2,0,NULL,'2016-11-13 10:10:57','2016-11-13 10:10:57'),(62,42,4,2,0,NULL,'2016-11-13 10:10:57','2016-11-13 10:10:57'),(63,42,8,2,0,NULL,'2016-11-13 10:10:57','2016-11-13 10:10:57'),(64,43,4,2,0,NULL,'2016-11-13 10:10:58','2016-11-13 10:10:58'),(65,43,8,2,0,NULL,'2016-11-13 10:10:58','2016-11-13 10:10:58'),(66,44,4,2,0,NULL,'2016-11-13 10:10:58','2016-11-13 10:10:58'),(67,44,8,2,0,NULL,'2016-11-13 10:10:58','2016-11-13 10:10:58'),(68,45,4,2,0,NULL,'2016-11-13 10:10:59','2016-11-13 10:10:59'),(69,45,8,2,0,NULL,'2016-11-13 10:10:59','2016-11-13 10:10:59'),(70,46,4,2,0,NULL,'2016-11-13 10:10:59','2016-11-13 10:10:59'),(71,46,8,2,0,NULL,'2016-11-13 10:10:59','2016-11-13 10:10:59'),(72,47,4,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(73,47,8,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(74,48,4,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(75,48,8,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(76,49,4,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(77,49,8,2,0,NULL,'2016-11-13 10:11:00','2016-11-13 10:11:00'),(78,50,4,2,0,NULL,'2016-11-13 10:25:53','2016-11-13 10:25:53'),(79,50,8,4,0,NULL,'2016-11-13 10:25:53','2016-11-13 10:25:53'),(80,51,4,2,0,NULL,'2016-11-13 10:26:10','2016-11-13 10:26:10'),(81,51,8,4,0,NULL,'2016-11-13 10:26:10','2016-11-13 10:26:10'),(82,52,4,2,0,NULL,'2016-11-13 10:26:12','2016-11-13 10:26:12'),(83,52,8,4,0,NULL,'2016-11-13 10:26:12','2016-11-13 10:26:12'),(84,53,4,2,0,NULL,'2016-11-13 10:26:14','2016-11-13 10:26:14'),(85,53,8,4,0,NULL,'2016-11-13 10:26:14','2016-11-13 10:26:14'),(86,54,8,3,0,NULL,'2016-11-13 10:30:02','2016-11-13 10:30:02'),(87,55,8,3,0,NULL,'2016-11-13 10:30:42','2016-11-13 10:30:42'),(88,55,9,1,0,NULL,'2016-11-13 10:30:42','2016-11-13 10:30:42'),(89,56,8,3,0,NULL,'2016-11-13 10:30:42','2016-11-13 10:30:42'),(90,56,9,1,0,NULL,'2016-11-13 10:30:42','2016-11-13 10:30:42'),(91,57,8,3,0,NULL,'2016-11-13 10:30:43','2016-11-13 10:30:43'),(92,57,9,1,0,NULL,'2016-11-13 10:30:43','2016-11-13 10:30:43'),(93,58,8,3,0,NULL,'2016-11-13 10:30:44','2016-11-13 10:30:44'),(94,58,9,1,0,NULL,'2016-11-13 10:30:44','2016-11-13 10:30:44'),(95,59,8,3,0,NULL,'2016-11-13 10:30:46','2016-11-13 10:30:46'),(96,59,9,1,0,NULL,'2016-11-13 10:30:46','2016-11-13 10:30:46'),(97,60,8,3,0,NULL,'2016-11-13 10:30:48','2016-11-13 10:30:48'),(98,60,9,1,0,NULL,'2016-11-13 10:30:48','2016-11-13 10:30:48'),(99,61,8,3,0,NULL,'2016-11-13 10:30:49','2016-11-13 10:30:49'),(100,61,9,1,0,NULL,'2016-11-13 10:30:49','2016-11-13 10:30:49'),(101,62,8,3,0,NULL,'2016-11-13 10:30:49','2016-11-13 10:30:49'),(102,62,9,1,0,NULL,'2016-11-13 10:30:49','2016-11-13 10:30:49'),(103,63,8,3,0,NULL,'2016-11-13 10:30:50','2016-11-13 10:30:50'),(104,63,9,1,0,NULL,'2016-11-13 10:30:50','2016-11-13 10:30:50'),(105,64,8,3,0,NULL,'2016-11-13 10:30:51','2016-11-13 10:30:51'),(106,64,9,1,0,NULL,'2016-11-13 10:30:51','2016-11-13 10:30:51'),(107,65,8,3,0,NULL,'2016-11-13 10:30:53','2016-11-13 10:30:53'),(108,65,9,1,0,NULL,'2016-11-13 10:30:53','2016-11-13 10:30:53'),(109,66,8,3,0,NULL,'2016-11-13 11:56:56','2016-11-13 11:56:56'),(110,66,9,1,0,NULL,'2016-11-13 11:56:56','2016-11-13 11:56:56'),(111,67,8,3,0,NULL,'2016-11-13 11:56:57','2016-11-13 11:56:57'),(112,67,9,1,0,NULL,'2016-11-13 11:56:57','2016-11-13 11:56:57'),(113,68,8,3,0,NULL,'2016-11-13 11:57:00','2016-11-13 11:57:00'),(114,68,9,1,0,NULL,'2016-11-13 11:57:00','2016-11-13 11:57:00'),(115,69,8,3,0,NULL,'2016-11-13 11:57:01','2016-11-13 11:57:01'),(116,69,9,1,0,NULL,'2016-11-13 11:57:01','2016-11-13 11:57:01'),(117,70,8,3,0,NULL,'2016-11-13 11:57:02','2016-11-13 11:57:02'),(118,70,9,1,0,NULL,'2016-11-13 11:57:02','2016-11-13 11:57:02'),(119,71,8,3,0,NULL,'2016-11-13 11:57:02','2016-11-13 11:57:02'),(120,71,9,1,0,NULL,'2016-11-13 11:57:02','2016-11-13 11:57:02'),(121,72,8,3,0,NULL,'2016-11-13 11:57:03','2016-11-13 11:57:03'),(122,72,9,1,0,NULL,'2016-11-13 11:57:03','2016-11-13 11:57:03'),(123,73,8,3,0,NULL,'2016-11-13 11:57:03','2016-11-13 11:57:03'),(124,73,9,1,0,NULL,'2016-11-13 11:57:03','2016-11-13 11:57:03'),(125,74,8,3,0,NULL,'2016-11-13 11:57:04','2016-11-13 11:57:04'),(126,74,9,1,0,NULL,'2016-11-13 11:57:04','2016-11-13 11:57:04'),(127,75,8,3,0,NULL,'2016-11-13 11:57:04','2016-11-13 11:57:04'),(128,75,9,1,0,NULL,'2016-11-13 11:57:04','2016-11-13 11:57:04'),(129,76,8,3,0,NULL,'2016-11-13 11:57:05','2016-11-13 11:57:05'),(130,76,9,1,0,NULL,'2016-11-13 11:57:05','2016-11-13 11:57:05'),(131,77,8,3,0,NULL,'2016-11-13 11:57:05','2016-11-13 11:57:05'),(132,77,9,1,0,NULL,'2016-11-13 11:57:05','2016-11-13 11:57:05'),(133,78,8,3,0,NULL,'2016-11-13 11:57:06','2016-11-13 11:57:06'),(134,78,9,1,0,NULL,'2016-11-13 11:57:06','2016-11-13 11:57:06'),(135,79,8,3,0,NULL,'2016-11-13 11:57:06','2016-11-13 11:57:06'),(136,79,9,1,0,NULL,'2016-11-13 11:57:06','2016-11-13 11:57:06'),(137,80,8,3,0,NULL,'2016-11-13 11:57:07','2016-11-13 11:57:07'),(138,80,9,1,0,NULL,'2016-11-13 11:57:07','2016-11-13 11:57:07'),(139,81,8,3,0,NULL,'2016-11-13 11:57:07','2016-11-13 11:57:07'),(140,81,9,1,0,NULL,'2016-11-13 11:57:07','2016-11-13 11:57:07'),(141,82,8,3,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(142,82,9,1,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(143,83,8,3,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(144,83,9,1,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(145,84,8,3,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(146,84,9,1,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(147,85,8,3,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(148,85,9,1,0,NULL,'2016-11-13 11:57:08','2016-11-13 11:57:08'),(149,86,8,3,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(150,86,9,1,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(151,87,8,3,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(152,87,9,1,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(153,88,8,3,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(154,88,9,1,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(155,89,8,3,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(156,89,9,1,0,NULL,'2016-11-13 11:57:09','2016-11-13 11:57:09'),(157,90,8,3,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(158,90,9,1,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(159,91,8,3,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(160,91,9,1,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(161,92,8,3,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(162,92,9,1,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(163,93,8,3,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(164,93,9,1,0,NULL,'2016-11-13 11:57:10','2016-11-13 11:57:10'),(165,94,8,3,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(166,94,9,1,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(167,95,8,3,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(168,95,9,1,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(169,96,8,3,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(170,96,9,1,0,NULL,'2016-11-13 11:57:11','2016-11-13 11:57:11'),(171,97,8,3,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(172,97,9,1,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(173,98,8,3,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(174,98,9,1,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(175,99,8,3,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(176,99,9,1,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(177,100,8,3,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(178,100,9,1,0,NULL,'2016-11-13 11:57:12','2016-11-13 11:57:12'),(179,101,8,3,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(180,101,9,1,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(181,102,8,3,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(182,102,9,1,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(183,103,8,3,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(184,103,9,1,0,NULL,'2016-11-13 11:57:13','2016-11-13 11:57:13'),(185,104,8,3,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(186,104,9,1,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(187,105,8,3,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(188,105,9,1,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(189,106,8,3,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(190,106,9,1,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(191,107,8,3,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(192,107,9,1,0,NULL,'2016-11-13 11:57:14','2016-11-13 11:57:14'),(193,108,8,3,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(194,108,9,1,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(195,109,8,3,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(196,109,9,1,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(197,110,8,3,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(198,110,9,1,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(199,111,8,3,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(200,111,9,1,0,NULL,'2016-11-13 11:57:15','2016-11-13 11:57:15'),(201,112,8,3,0,NULL,'2016-11-13 11:57:21','2016-11-13 11:57:21'),(202,112,9,1,0,NULL,'2016-11-13 11:57:21','2016-11-13 11:57:21'),(203,113,8,3,0,NULL,'2016-11-13 11:57:23','2016-11-13 11:57:23'),(204,113,9,1,0,NULL,'2016-11-13 11:57:23','2016-11-13 11:57:23'),(205,114,4,1,0,NULL,'2016-11-14 06:06:03','2016-11-14 06:06:03'),(206,115,8,2,0,NULL,'2016-11-14 06:07:57','2016-11-14 06:07:57'),(207,116,8,2,0,NULL,'2016-11-14 06:08:12','2016-11-14 06:08:12'),(208,117,8,2,0,NULL,'2016-11-14 06:08:23','2016-11-14 06:08:23'),(209,118,8,2,0,NULL,'2016-11-14 06:10:06','2016-11-14 06:10:06'),(210,119,8,2,0,NULL,'2016-11-14 06:10:08','2016-11-14 06:10:08'),(211,120,8,2,0,NULL,'2016-11-14 06:10:18','2016-11-14 06:10:18'),(212,121,8,2,0,NULL,'2016-11-14 06:10:23','2016-11-14 06:10:23'),(213,122,4,1,0,NULL,'2016-11-14 06:12:26','2016-11-14 06:12:26'),(214,123,4,1,0,NULL,'2016-11-14 06:12:44','2016-11-14 06:12:44'),(215,124,4,2,0,NULL,'2016-11-14 06:13:51','2016-11-14 06:13:51'),(216,125,4,2,0,NULL,'2016-11-14 06:13:54','2016-11-14 06:13:54'),(217,126,4,2,0,NULL,'2016-11-14 06:13:55','2016-11-14 06:13:55'),(218,127,4,2,0,NULL,'2016-11-14 06:14:35','2016-11-14 06:14:35'),(219,128,4,2,0,NULL,'2016-11-14 06:14:44','2016-11-14 06:14:44'),(220,128,8,1,0,NULL,'2016-11-14 06:14:44','2016-11-14 06:14:44'),(221,129,4,1,0,NULL,'2016-11-14 07:37:42','2016-11-14 07:37:42'),(222,130,4,2,0,NULL,'2016-11-14 14:50:53','2016-11-14 14:50:53'),(223,131,4,2,0,NULL,'2016-11-14 14:51:06','2016-11-14 14:51:06'),(224,132,4,1,0,NULL,'2016-11-15 09:23:55','2016-11-15 09:23:55'),(225,132,8,1,0,NULL,'2016-11-15 09:23:55','2016-11-15 09:23:55'),(226,132,9,1,0,NULL,'2016-11-15 09:23:55','2016-11-15 09:23:55'),(227,133,4,3,0,NULL,'2016-11-15 09:46:41','2016-11-15 09:46:41'),(228,133,8,3,0,NULL,'2016-11-15 09:46:41','2016-11-15 09:46:41'),(229,134,4,3,0,NULL,'2016-11-15 10:14:02','2016-11-15 10:14:02'),(230,134,8,3,0,NULL,'2016-11-15 10:14:02','2016-11-15 10:14:02'),(231,135,4,3,0,NULL,'2016-11-15 10:14:26','2016-11-15 10:14:26'),(232,135,8,3,0,NULL,'2016-11-15 10:14:26','2016-11-15 10:14:26'),(233,136,4,2,0,NULL,'2016-11-15 15:01:00','2016-11-15 15:01:00'),(234,136,8,2,0,NULL,'2016-11-15 15:01:00','2016-11-15 15:01:00'),(235,136,9,1,0,NULL,'2016-11-15 15:01:00','2016-11-15 15:01:00'),(236,137,4,1,0,NULL,'2016-11-15 15:03:27','2016-11-15 15:03:27'),(237,138,4,2,0,NULL,'2016-11-16 10:25:59','2016-11-16 10:25:59'),(238,138,8,2,0,NULL,'2016-11-16 10:25:59','2016-11-16 10:25:59'),(239,138,9,1,0,NULL,'2016-11-16 10:25:59','2016-11-16 10:25:59'),(240,139,4,1,0,NULL,'2016-11-16 10:30:06','2016-11-16 10:30:06'),(241,139,8,2,0,NULL,'2016-11-16 10:30:06','2016-11-16 10:30:06'),(242,140,4,2,0,NULL,'2016-11-16 10:49:04','2016-11-16 10:49:04'),(243,140,8,2,0,NULL,'2016-11-16 10:49:04','2016-11-16 10:49:04'),(244,140,9,2,0,NULL,'2016-11-16 10:49:04','2016-11-16 10:49:04'),(245,141,4,1,0,NULL,'2016-11-16 11:48:26','2016-11-16 11:48:26'),(246,142,4,1,0,NULL,'2016-11-16 12:12:57','2016-11-16 12:12:57'),(247,143,8,2,0,NULL,'2016-11-16 15:17:59','2016-11-16 15:17:59'),(248,143,9,2,0,NULL,'2016-11-16 15:17:59','2016-11-16 15:17:59'),(249,144,4,3,0,NULL,'2016-11-16 16:41:21','2016-11-16 16:41:21'),(250,144,8,2,0,NULL,'2016-11-16 16:41:21','2016-11-16 16:41:21'),(267,155,4,1,0,NULL,'2016-11-17 13:30:23','2016-11-17 13:30:23'),(268,155,9,1,0,NULL,'2016-11-17 13:30:23','2016-11-17 13:30:23'),(269,157,4,2,0,156,'2016-11-17 13:30:28','2016-11-17 14:43:33'),(270,157,4,1,0,NULL,'2016-11-17 13:30:35','2016-11-17 13:30:35'),(271,157,8,2,0,NULL,'2016-11-17 13:30:35','2016-11-17 13:30:35'),(272,158,4,2,0,NULL,'2016-11-18 04:58:19','2016-11-18 04:58:19'),(273,158,8,2,0,NULL,'2016-11-18 04:58:19','2016-11-18 04:58:19'),(274,158,9,1,0,NULL,'2016-11-18 04:58:19','2016-11-18 04:58:19'),(275,159,4,1,0,NULL,'2016-11-18 04:58:49','2016-11-18 04:58:49'),(276,159,8,1,0,NULL,'2016-11-18 04:58:49','2016-11-18 04:58:49'),(277,159,9,1,0,NULL,'2016-11-18 04:58:49','2016-11-18 04:58:49'),(282,158,4,2,1,NULL,NULL,NULL),(283,158,8,1,1,NULL,NULL,NULL),(284,158,9,1,1,NULL,NULL,NULL),(285,158,4,2,1,NULL,NULL,NULL),(286,158,8,1,1,NULL,NULL,NULL),(287,158,9,1,1,NULL,NULL,NULL),(288,158,4,2,1,NULL,NULL,NULL),(289,158,4,1,1,NULL,NULL,NULL),(290,158,4,1,1,NULL,NULL,NULL),(291,160,4,2,0,NULL,'2016-11-18 06:56:54','2016-11-18 06:56:54'),(292,160,8,1,0,NULL,'2016-11-18 06:56:54','2016-11-18 06:56:54'),(293,160,4,1,1,NULL,NULL,NULL),(294,160,4,2,1,NULL,NULL,NULL),(295,160,8,2,1,NULL,NULL,NULL),(296,161,4,2,0,NULL,'2016-11-18 12:26:59','2016-11-18 12:26:59'),(297,161,8,1,0,NULL,'2016-11-18 12:26:59','2016-11-18 12:26:59'),(298,161,4,1,1,NULL,NULL,NULL),(299,161,8,1,1,NULL,NULL,NULL),(300,162,4,2,0,NULL,'2016-11-18 13:59:05','2016-11-18 13:59:05'),(301,162,8,1,0,NULL,'2016-11-18 13:59:05','2016-11-18 13:59:05'),(302,163,4,2,0,NULL,'2016-11-18 18:00:01','2016-11-18 18:00:01'),(303,164,4,1,0,NULL,'2016-11-21 08:01:42','2016-11-21 08:01:42'),(304,165,8,2,0,NULL,'2016-11-24 07:30:24','2016-11-24 07:30:24');
/*!40000 ALTER TABLE `customer_orders_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_saveds`
--

DROP TABLE IF EXISTS `customer_saveds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_saveds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_saveds`
--

LOCK TABLES `customer_saveds` WRITE;
/*!40000 ALTER TABLE `customer_saveds` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_saveds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) DEFAULT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) NOT NULL DEFAULT '1',
  `avata` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `socialite_id` bigint(20) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_staff` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,6,'staff','staff@gmail.com','Nguyễn Trọng Hiếu',1,'aa23aeca03fe2dccb280c78242364ede.jpg','9819451750',NULL,'$2y$10$l5FydGcPRn0hTEZd.QdPsOEPae9rSdfmY4pTVll6Ynt9RyWr.Hc/a',1,1,'rbnrx9eq2yDMF0y45PrlHHG96GWOEcUSRxUwm6lQpCANDYiSj6tMkXZ39PRD','2016-11-09 11:08:33','2016-11-13 06:42:37'),(3,6,'chienma123dondoc','chienma123dondoc@gmail.com','Hieu Nguyen',0,'4fb9ded236b60014142399b2b6aa0c0c.jpg','9819451751',NULL,'$2y$10$5brC1RT11PTPtEdTdJ2bx./UGgHR95/4/muD6Pqm5/q7AglzzdIhy',1,1,NULL,'2016-11-09 18:55:59','2016-11-10 12:33:06'),(4,NULL,'chienmadon123123123doc','chienmadon123123123doc@gmail.com','Hieu Nguyen',1,NULL,'9819451751',NULL,'$2y$10$YDAHBfar.9cNkkqocFCKOukn60yurtnQjIiEbgRtCiroXss9.JCMe',1,0,NULL,'2016-11-09 18:56:16','2016-11-10 12:33:14');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_banners`
--

DROP TABLE IF EXISTS `member_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_banners` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_banners`
--

LOCK TABLES `member_banners` WRITE;
/*!40000 ALTER TABLE `member_banners` DISABLE KEYS */;
INSERT INTO `member_banners` VALUES (5,6,'60267bcaf9b7d9c2c865eb56164ebfca.jpg','2016-11-09 11:50:26','2016-11-09 11:50:26'),(6,6,'debad18506e99ab521ecf9fe75944913.jpg','2016-11-09 11:50:26','2016-11-09 11:50:26');
/*!40000 ALTER TABLE `member_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_costs`
--

DROP TABLE IF EXISTS `member_costs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_costs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_costs`
--

LOCK TABLES `member_costs` WRITE;
/*!40000 ALTER TABLE `member_costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_costs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_import_goods`
--

DROP TABLE IF EXISTS `member_import_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_import_goods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_id` bigint(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_import_goods`
--

LOCK TABLES `member_import_goods` WRITE;
/*!40000 ALTER TABLE `member_import_goods` DISABLE KEYS */;
INSERT INTO `member_import_goods` VALUES (3,6,'Gà','ga',1,1,'2016-10-31 11:32:25','2016-10-31 11:32:25'),(4,6,'Heo','heo',1,1,'2016-11-06 06:22:11','2016-11-06 06:22:11'),(5,6,'Mực','muc',1,1,'2016-11-06 06:27:54','2016-11-06 06:27:54'),(6,6,'Tôm','tom',1,1,'2016-11-10 05:30:58','2016-11-10 05:30:58');
/*!40000 ALTER TABLE `member_import_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_import_goods_ofupdate`
--

DROP TABLE IF EXISTS `member_import_goods_ofupdate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_import_goods_ofupdate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `update_id` bigint(20) NOT NULL,
  `price` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_import_goods_ofupdate`
--

LOCK TABLES `member_import_goods_ofupdate` WRITE;
/*!40000 ALTER TABLE `member_import_goods_ofupdate` DISABLE KEYS */;
INSERT INTO `member_import_goods_ofupdate` VALUES (1,16,70000,50,'2016-10-31 14:08:31','2016-10-31 14:08:31');
/*!40000 ALTER TABLE `member_import_goods_ofupdate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_import_goods_price`
--

DROP TABLE IF EXISTS `member_import_goods_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_import_goods_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `import_id` bigint(20) NOT NULL,
  `price` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_import_goods_price`
--

LOCK TABLES `member_import_goods_price` WRITE;
/*!40000 ALTER TABLE `member_import_goods_price` DISABLE KEYS */;
INSERT INTO `member_import_goods_price` VALUES (18,3,60000,1,'2016-10-31 11:32:25','2016-10-31 11:32:25'),(20,3,50000,0,'2016-10-31 11:34:18','2016-10-31 11:34:18'),(21,3,60000,1,'2016-10-31 11:53:58','2016-10-31 11:53:58'),(22,3,80000,1,'2016-10-31 11:54:33','2016-10-31 11:54:33'),(23,3,70000,0,'2016-10-31 14:07:08','2016-10-31 14:07:08'),(24,3,80000,1,'2016-10-31 18:07:35','2016-10-31 18:07:35'),(25,4,100000,1,'2016-11-06 06:22:11','2016-11-06 06:22:11'),(26,4,120000,1,'2016-11-06 06:22:41','2016-11-06 06:22:41'),(27,5,50000,1,'2016-11-06 06:27:54','2016-11-06 06:27:54'),(28,6,100000,1,'2016-11-10 05:30:58','2016-11-10 05:30:58');
/*!40000 ALTER TABLE `member_import_goods_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_import_goods_update`
--

DROP TABLE IF EXISTS `member_import_goods_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_import_goods_update` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `import_id` bigint(20) NOT NULL,
  `price_id` bigint(20) NOT NULL,
  `quality` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_import_goods_update`
--

LOCK TABLES `member_import_goods_update` WRITE;
/*!40000 ALTER TABLE `member_import_goods_update` DISABLE KEYS */;
INSERT INTO `member_import_goods_update` VALUES (12,3,18,40,'2016-10-31 11:35:27','2016-10-31 11:32:25'),(13,3,21,20,'2016-10-31 11:53:58','2016-10-31 11:53:58'),(14,3,21,80,'2016-10-31 11:54:03','2016-10-31 11:54:03'),(15,3,21,50,'2016-10-31 11:54:17','2016-10-30 11:54:17'),(16,3,22,50,'2016-10-31 14:08:31','2016-10-31 11:54:33'),(17,3,24,20,'2016-10-31 18:07:35','2016-10-31 18:07:35'),(18,3,24,20,'2016-11-06 06:21:32','2016-11-06 06:21:32'),(19,4,25,10,'2016-11-06 06:22:11','2016-11-06 06:22:11'),(20,4,25,20,'2016-11-06 06:22:25','2016-11-06 06:22:25'),(21,4,26,10,'2016-11-06 06:22:41','2016-11-06 06:22:41'),(22,5,27,10,'2016-11-06 06:27:54','2016-11-06 06:27:54'),(23,5,27,20,'2016-11-06 10:49:37','2016-11-06 10:49:37'),(24,5,27,15,'2016-11-06 10:50:24','2016-11-06 10:50:24'),(25,6,28,50,'2016-11-10 05:30:58','2016-11-10 05:30:58');
/*!40000 ALTER TABLE `member_import_goods_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_list_tables`
--

DROP TABLE IF EXISTS `member_list_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_list_tables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_list_tables`
--

LOCK TABLES `member_list_tables` WRITE;
/*!40000 ALTER TABLE `member_list_tables` DISABLE KEYS */;
INSERT INTO `member_list_tables` VALUES (23,'A01',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(24,'A02',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(25,'A03',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(26,'A04',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(27,'A05',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(28,'A06',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(29,'A07',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(30,'A08',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(31,'A09',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(32,'A10',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(33,'A11',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(34,'A12',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(35,'A13',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(36,'A14',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(37,'A15',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(38,'A16',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(39,'A17',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(40,'A18',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(41,'A19',6,1,'2016-11-13 14:32:49','2016-11-13 14:32:49'),(42,'A20',6,0,'2016-11-13 14:32:49','2016-11-13 14:39:01');
/*!40000 ALTER TABLE `member_list_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_menus`
--

DROP TABLE IF EXISTS `member_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` bigint(20) NOT NULL,
  `is_price` int(11) NOT NULL DEFAULT '1',
  `is_show` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_menus`
--

LOCK TABLES `member_menus` WRITE;
/*!40000 ALTER TABLE `member_menus` DISABLE KEYS */;
INSERT INTO `member_menus` VALUES (4,6,'Gà nướng','ga-nuong',1,1,1,1,'2016-10-28 09:44:35','2016-11-18 12:59:50'),(8,6,'Gà luộc 1','ga-luoc-1',1,1,1,1,'2016-10-29 02:27:35','2016-11-18 12:59:43'),(9,6,'Heo quay','heo-quay',1,1,1,1,'2016-10-29 02:42:44','2016-11-10 05:42:25');
/*!40000 ALTER TABLE `member_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_menus_price`
--

DROP TABLE IF EXISTS `member_menus_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_menus_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_menus_price`
--

LOCK TABLES `member_menus_price` WRITE;
/*!40000 ALTER TABLE `member_menus_price` DISABLE KEYS */;
INSERT INTO `member_menus_price` VALUES (1,4,100000,'2016-10-29 08:58:30','0000-00-00 00:00:00'),(2,8,10000022,'2017-03-28 15:30:18','2016-10-29 02:27:35'),(3,8,1000000,'2016-10-29 02:29:43','2016-10-29 02:29:43'),(4,8,1500000,'2016-10-29 02:40:38','2016-10-29 02:40:38'),(5,9,1000000,'2016-10-29 02:42:44','2016-10-29 02:42:44'),(6,9,2000000,'2016-10-29 02:43:01','2016-10-29 02:43:01'),(7,9,800000,'2016-10-29 10:11:41','2016-10-29 10:11:41'),(8,9,8000000,'2016-10-29 10:36:55','2016-10-29 10:36:55'),(9,9,100000,'2016-10-31 10:22:05','2016-10-31 10:22:05'),(10,9,100000,'2016-10-31 10:25:09','2016-10-31 10:25:09'),(11,8,1500000,'2016-10-31 11:09:54','2016-10-31 11:09:54'),(12,8,1500000,'2016-10-31 11:10:31','2016-10-31 11:10:31');
/*!40000 ALTER TABLE `member_menus_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_orders`
--

DROP TABLE IF EXISTS `member_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `quality` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_orders`
--

LOCK TABLES `member_orders` WRITE;
/*!40000 ALTER TABLE `member_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_profiles`
--

DROP TABLE IF EXISTS `member_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` text COLLATE utf8_unicode_ci,
  `contact_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `time_open` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_close` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '0',
  `comments` int(11) NOT NULL DEFAULT '0',
  `saveds` int(11) NOT NULL DEFAULT '0',
  `likeds` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_profiles`
--

LOCK TABLES `member_profiles` WRITE;
/*!40000 ALTER TABLE `member_profiles` DISABLE KEYS */;
INSERT INTO `member_profiles` VALUES (2,6,'F Zone','hieu-nguyen','966d552d892b368ccddb60cf74029613.jpg','126 Lê Thạch, Hòa Minh, Đà Nẵng, Việt Nam','126-le-thach-hoa-minh-da-nang-viet-nam','Đà Nẵng','da-nang','16.0557841','108.17223939999997',NULL,'0981945175','Nhà hàng vip nhất đà nẵng','7:00 AM','22:00 PM',1,0,0,0,0,'2016-10-28 01:25:00','2016-11-18 09:28:22'),(3,8,'Hieu Nguyen','hieu-nguyen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0981945175',NULL,NULL,NULL,1,0,0,0,0,'2016-11-04 06:55:13','2016-11-04 06:55:13'),(4,9,'Hieu Nguyen','hieu-nguyen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0981945175',NULL,NULL,NULL,1,0,0,0,0,'2016-11-04 06:58:48','2016-11-04 06:58:48'),(5,10,'Hieu Nguyen','hieu-nguyen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0981945175',NULL,NULL,NULL,1,0,0,0,0,'2016-11-04 06:59:26','2016-11-04 06:59:26'),(6,11,'Hieu Nguyen','hieu-nguyen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0981945175',NULL,NULL,NULL,1,0,0,0,0,'2016-11-04 07:00:27','2016-11-04 07:00:27');
/*!40000 ALTER TABLE `member_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_rank`
--

DROP TABLE IF EXISTS `member_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `comment` int(11) NOT NULL DEFAULT '0',
  `liked` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_rank`
--

LOCK TABLES `member_rank` WRITE;
/*!40000 ALTER TABLE `member_rank` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_rank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_staffs`
--

DROP TABLE IF EXISTS `member_staffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_staffs`
--

LOCK TABLES `member_staffs` WRITE;
/*!40000 ALTER TABLE `member_staffs` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_staffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_tables`
--

DROP TABLE IF EXISTS `member_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_tables` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_tables`
--

LOCK TABLES `member_tables` WRITE;
/*!40000 ALTER TABLE `member_tables` DISABLE KEYS */;
INSERT INTO `member_tables` VALUES (1,6,'Bàn gia đình','56a0abac4d8657fd228e8c2e211bab8d.jpg','Bàn gia đình 4-6 người.\r\n',1,'2016-11-01 16:48:00','2016-11-09 11:41:09'),(2,6,'1',NULL,'1',0,'2016-11-01 16:48:29','2016-11-01 16:48:32'),(3,6,'Bàn buffe','a1dcda58b05f34c53beb9f29ad05eb65.jpg','bàn vip',1,'2016-11-06 11:05:16','2016-11-06 11:30:14'),(4,6,'Bàn pro','d3e7e13776294cf7c58641398d03c372.jpg','Oke',1,'2016-11-06 11:36:32','2016-11-09 18:52:48');
/*!40000 ALTER TABLE `member_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_ads` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `members_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (6,'tronghieudev','tronghieudev@gmail.com','$2y$10$4GrICekuz2O3oPxJDfLzE.NsF645AezFasUxSMWvnKtuzu8P/eCn.',1,'V0wZrZDOS5yvhwmkcihpcp0ivgk5oNbUVBH7luOJHwwylt6RmxvPKxGEsavN','2016-10-28 01:25:00','2016-11-18 08:57:48',0),(8,'chienmadondoc','chienmadondoc@gmail.com','$2y$10$2tpBWryGK2EhaCQRJQSIpuUywkf0SbceI0V7k/Bj1L2RINQbgPtR2',1,'uZoUpfa4px2wUpDqzBKpL96rDYKUZiGoNr6Yey4E9tQIfoiPwliuK46g2ZkY','2016-11-04 06:55:13','2016-11-15 09:21:37',0),(9,'chienmadondoc123','chienmadondoc123@gmail.com','$2y$10$10w6iGkLQJ8WeJCI/iDVdOqG5Y12.TedZxV12dciy3sykE3a5kIsC',1,NULL,'2016-11-04 06:58:48','2016-11-04 06:58:48',0),(10,'chienmadondoc123456','chienmadondoc123456@gmail.com','$2y$10$7qd5ZQBwmIfg9CkB/8ytH.E9VOECTuqHej1NUvgoVOQeCuYZyQCUW',1,'5yBcBcvLOKZsV7QF38ZNc0KEHT7kYhDosuwMvjWfxfSrCJSY1pr4qopo8Slo','2016-11-04 06:59:26','2016-11-04 07:00:15',0),(11,'chienmadondoc321','chienmadondoc321@gmail.com','$2y$10$.SZ9gMndpbXoysohHLiLKO6gNLC1ozO7dJ5XnRY.aALdoLfGQFN26',1,'imgqstvmwukcw3T7XZzLrOyFZZsqQl6JQ8WCWw32GlWZdozL6iFAPKt1GFfp','2016-11-04 07:00:27','2016-11-04 07:06:18',0);
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (19,'2014_10_12_000000_create_users_table',1),(20,'2014_10_12_100000_create_password_resets_table',1),(21,'2016_10_26_134945_create_members_table',1),(22,'2016_10_26_135036_create_member_profiles_table',1),(23,'2016_10_26_135059_create_member_menus_table',1),(24,'2016_10_26_135123_create_member_tables_table',1),(25,'2016_10_26_135146_create_member_orders_table',1),(26,'2016_10_26_135219_create_member_staffs_table',1),(27,'2016_10_26_135310_create_member_costs_table',1),(28,'2016_10_26_135503_create_member_import_goods_table',1),(29,'2016_10_26_135557_create_customers_table',1),(30,'2016_10_26_135613_create_customer_orders_table',1),(31,'2016_10_26_135633_create_customer_orders_detail_table',1),(32,'2016_10_26_135649_create_customer_comments_table',1),(33,'2016_10_26_143921_create_customer_units_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifies`
--

DROP TABLE IF EXISTS `notifies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifies` (
  `id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `cmt_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `is_view` int(11) NOT NULL DEFAULT '0',
  `is_see` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifies`
--

LOCK TABLES `notifies` WRITE;
/*!40000 ALTER TABLE `notifies` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `content` text,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,6,NULL,'Marionette.js. View. Defining childViewEvents','marionettejs-view-defining-childviewevents','eb04df80cff6f43853592f53c611167a.jpg','a',NULL,0,'2016-11-03 21:27:24','2016-11-03 21:38:35'),(2,6,NULL,'Marionette.js. View. Defining childViewEvents','marionettejs-view-defining-childviewevents','79e3228ffbbda81fbd9e9b0908e6df35.jpg','ádfasdf',NULL,0,'2016-11-03 21:28:02','2016-11-03 21:38:50'),(3,6,NULL,'Món ngon mùa hè.','mon-ngon-mua-he','606ea70a30523d973077d61e009fb933.jpg','If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.','<pre>\r\nIf the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</pre>\r\n\r\n<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n\r\n<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n\r\n<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n',1,'2016-11-03 21:30:01','2016-11-03 21:46:19'),(4,6,NULL,'If the search parameter is a string and the type parameter is set to TRUE','if-the-search-parameter-is-a-string-and-the-type-parameter-is-set-to-true','15deea61cc4b898596a8dd0a881f951c.jpg','If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.','<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n\r\n<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n\r\n<p>If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.</p>\r\n',1,'2016-11-03 21:48:08','2016-11-03 22:33:26');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_details`
--

DROP TABLE IF EXISTS `sale_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sale_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `quality` int(3) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_details`
--

LOCK TABLES `sale_details` WRITE;
/*!40000 ALTER TABLE `sale_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `is_success` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL DEFAULT '0',
  `table_id` bigint(20) NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,6,'kilogam','Kg',1,'2016-10-27 17:00:00','2016-10-27 17:00:00'),(2,6,'Cái','c',0,'2016-10-28 21:38:47','2016-10-28 21:39:37'),(3,6,'Gói','Gới',1,'2016-10-28 21:39:56','2016-10-28 21:39:56'),(4,11,'kilogam','kg',1,'2016-11-04 07:00:27','2016-11-04 07:00:27');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-02 11:24:32
